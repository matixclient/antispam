package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.chat.ReceiveChatEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.utils.chat.StringUtils;

import net.minecraft.client.gui.ChatLine;
import net.minecraft.client.gui.GuiNewChat;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Lars on 26.07.2016.
 */
public class ModuleAntiSpam extends Module {
  private Field chatLines;

  public ModuleAntiSpam() {
    super("AntiSpam", ModuleCategory.OTHER);

    this.setEnabled(true);
    this.setDisplayedInGui(false);
    this.setVersion("1.0");
    this.setBuildVersion(16001);
    this.setDescription("Stops others from spamming the chat");
    this.setSyntax("antispam");

    try {
      this.chatLines = GuiNewChat.class.getDeclaredField("drawnChatLines");
      this.chatLines.setAccessible(true);
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  @Override
  public void onEnable() {
    Wrapper.getEventManager().register(this);
  }

  @EventHandler
  public void onReceiveChat(ReceiveChatEvent chatEvent) {
    try {
      String chatMessage = chatEvent.getChatMessage();

      this.setChatLines(StringUtils.handleSpam(this.getChatLines(), chatEvent));
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }
  }

  private List<ChatLine> getChatLines() throws IllegalAccessException {
    return (List<ChatLine>) this.chatLines.get(Wrapper.getMinecraft().getIngameGUI().getChatGUI());
  }

  private void setChatLines(List<ChatLine> chatLines) throws IllegalAccessException {
    this.chatLines.set(Wrapper.getMinecraft().getIngameGUI().getChatGUI(), chatLines);
  }

  @Override
  public void onDisable() {
    Wrapper.getEventManager().unregister(this);
  }
}

