package de.paxii.clarinet.utils.chat;

import de.paxii.clarinet.event.events.chat.ReceiveChatEvent;
import de.paxii.clarinet.util.chat.ChatColor;

import net.minecraft.client.gui.ChatLine;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;

import java.lang.reflect.Field;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Lars on 26.07.2016.
 */
public class StringUtils {
  private static final Pattern SPAM_PATTERN = Pattern.compile("(?<=\\Q[\\E)\\d+(?=\\Qx]\\E)");

  public static List<ChatLine> handleSpam(List<ChatLine> chatLines, ReceiveChatEvent chatEvent) {
    String chatMessage = chatEvent.getChatMessage();
    String colorlessMessage = ChatColor.stripColor(chatMessage);

    for (ChatLine chatLine : chatLines) {
      String formattedText = ChatColor.stripColor(chatLine.getChatComponent().getFormattedText());

      if (formattedText.startsWith(colorlessMessage) && chatLines.indexOf(chatLine) <= 20) {
        int newMessageLength = colorlessMessage.length();
        int oldMessageLength = formattedText.length();

        if (newMessageLength <= oldMessageLength + 7 || newMessageLength >= oldMessageLength - 7) {
          TextComponentString newLine = new TextComponentString(chatMessage);
          Matcher spamMatcher = SPAM_PATTERN.matcher(formattedText);

          try {
            int spamAmount = spamMatcher.find() ? Integer.parseInt(spamMatcher.group()) + 1 : 2;
            newLine.appendText(String.format("%s [%dx]", ChatColor.GRAY, spamAmount));
          } catch (Exception e) {
            e.printStackTrace();
          }

          try {
            StringUtils.setChatComponent(chatLine, newLine);
          } catch (ReflectiveOperationException e) {
            e.printStackTrace();
          }

          chatEvent.setCancelled(true);
        }
      }
    }

    return chatLines;
  }

  private static void setChatComponent(ChatLine chatLine, ITextComponent textComponent) throws ReflectiveOperationException {
    Field lineString = ChatLine.class.getDeclaredField("lineString");
    lineString.setAccessible(true);

    lineString.set(chatLine, textComponent);
  }
}
